"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var customer_1 = require('../customers/customer');
function emailMatcher(c) {
    var emailControl = c.get('email');
    var confirmControl = c.get('confirmEmail');
    if (emailControl.pristine || confirmControl.pristine) {
        return null;
    }
    if (emailControl.value === confirmControl.value) {
        return null;
    }
    return { 'match': true };
}
// Using a validator function to accept parameters
// requires wrapping the validator function in a factory function
// the factory function takes in any desired parameters and returns a validator function
function ratingRange(min, max) {
    // returns an anonymous validator validator function to factory function
    return function (c) {
        if (c.value !== undefined &&
            (isNaN(c.value) || c.value < min || c.value > max)) {
            return { range: true };
        }
        return null;
    };
}
var CustomerReactive = (function () {
    // No need for manual creation of controls in form model when using formBuilder
    // firstName: FormControl = new FormControl();
    // lastName: FormControl = new FormControl();
    // email: FormControl = new FormControl();
    // sendCatalog: FormControl = new FormControl(true); // default value passed
    // addressType: FormControl = new FormControl();
    // street1: FormControl = new FormControl();
    // street2: FormControl = new FormControl();
    // city: FormControl = new FormControl();
    // state: FormControl = new FormControl();
    // zip: FormControl = new FormControl();
    function CustomerReactive(fb) {
        this.fb = fb;
        this.customer = new customer_1.Customer();
    }
    CustomerReactive.prototype.ngOnInit = function () {
        // Form Model - defines set of FormGroups and FormControls
        // that match up with HTML form and input elements
        this.customerForm = this.fb.group({
            firstName: ['', [forms_1.Validators.required, forms_1.Validators.minLength(3)]],
            lastName: ['', [forms_1.Validators.required, forms_1.Validators.maxLength(50)]],
            emailGroup: this.fb.group({
                email: [
                    '',
                    [forms_1.Validators.required, forms_1.Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+')]
                ],
                confirmEmail: ['', forms_1.Validators.required],
            }, { validator: emailMatcher }),
            phone: '',
            notification: 'email',
            rating: ['', ratingRange(1, 5)],
            sendCatalog: false
        });
        // this.customerForm = new FormGroup({
        //   firstName: this.firstName,
        //   lastName:  this.lastName,
        //   email: this.email,
        //   sendCatalog: this.sendCatalog,
        //   addressType: this.addressType,
        //   street1: this.street1,
        //   street2: this.street2,
        //   city: this.city,
        //   state: this.state,
        //   zip: this.zip
        // });
    };
    CustomerReactive.prototype.populateTestData = function () {
        this.customerForm.patchValue({
            firstName: 'Jack',
            lastName: 'Harkness',
            email: 'jack@torchwood.com',
            phone: '',
            notification: 'email',
            sendCatalog: false
        });
    };
    CustomerReactive.prototype.setNotification = function (notifyVia) {
        // const phoneControl  = this.customerForm.controls.phone;
        var phoneControl = this.customerForm.get('phone');
        if (notifyVia === 'text') {
            phoneControl.setValidators(forms_1.Validators.required);
        }
        else {
            phoneControl.clearValidators();
        }
        phoneControl.updateValueAndValidity();
    };
    CustomerReactive.prototype.save = function () {
        console.log(this.customerForm);
        console.log('Saved: ', JSON.stringify(this.customerForm.value));
    };
    CustomerReactive = __decorate([
        core_1.Component({
            selector: "reactive-signup",
            templateUrl: "./app/customers-reactive/customer-reactive.component.html"
        }), 
        __metadata('design:paramtypes', [forms_1.FormBuilder])
    ], CustomerReactive);
    return CustomerReactive;
}());
exports.CustomerReactive = CustomerReactive;
//# sourceMappingURL=customer-reactive.component.js.map