import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormControl,
  FormBuilder,
  Validators,
  AbstractControl,
  ValidatorFn
} from '@angular/forms';

import { Customer } from '../customers/customer';
import { StringMapWrapper } from '@angular/platform-browser/src/facade/collection';


function emailMatcher( c:AbstractControl ) {
  let emailControl = c.get('email');
  let confirmControl = c.get('confirmEmail');

  if ( emailControl.pristine || confirmControl.pristine ) {
    return null;
  }

  if ( emailControl.value === confirmControl.value ) {
    return null;
  }

  return {'match' : true}
}

// Using a validator function to accept parameters
// requires wrapping the validator function in a factory function
// the factory function takes in any desired parameters and returns a validator function
function ratingRange(min: number, max: number): ValidatorFn {
  // returns an anonymous validator validator function to factory function
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (
      c.value !== undefined &&
      (isNaN(c.value) || c.value < min || c.value > max)
    ) {
      return { range: true };
    }

    return null;
  };
}

@Component({
  selector: "reactive-signup",
  templateUrl: "./app/customers-reactive/customer-reactive.component.html"
})
export class CustomerReactive implements OnInit {
  customerForm: FormGroup;
  customer: Customer = new Customer();

  // No need for manual creation of controls in form model when using formBuilder
  // firstName: FormControl = new FormControl();
  // lastName: FormControl = new FormControl();
  // email: FormControl = new FormControl();
  // sendCatalog: FormControl = new FormControl(true); // default value passed
  // addressType: FormControl = new FormControl();
  // street1: FormControl = new FormControl();
  // street2: FormControl = new FormControl();
  // city: FormControl = new FormControl();
  // state: FormControl = new FormControl();
  // zip: FormControl = new FormControl();

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    // Form Model - defines set of FormGroups and FormControls
    // that match up with HTML form and input elements

    this.customerForm = this.fb.group({
      firstName: ['', [Validators.required, Validators.minLength(3)]],
      lastName: ['', [Validators.required, Validators.maxLength(50)]],
      emailGroup: this.fb.group({
        email: [
          '',
          [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+')]
        ],
        confirmEmail: ['', Validators.required],
      }, {validator: emailMatcher}),
      phone: '',
      notification: 'email',
      rating: ['', ratingRange(1, 5)],
      sendCatalog: false
    });

    // this.customerForm = new FormGroup({
    //   firstName: this.firstName,
    //   lastName:  this.lastName,
    //   email: this.email,
    //   sendCatalog: this.sendCatalog,
    //   addressType: this.addressType,
    //   street1: this.street1,
    //   street2: this.street2,
    //   city: this.city,
    //   state: this.state,
    //   zip: this.zip
    // });
  }

  populateTestData(): void {
    this.customerForm.patchValue({
      firstName: 'Jack',
      lastName: 'Harkness',
      email: 'jack@torchwood.com',
      phone: '',
      notification: 'email',
      sendCatalog: false
    });
  }

  setNotification(notifyVia: StringMapWrapper): void {
    // const phoneControl  = this.customerForm.controls.phone;
    const phoneControl: AbstractControl = this.customerForm.get('phone');

    if (notifyVia === 'text') {
      phoneControl.setValidators(Validators.required);
    } else {
      phoneControl.clearValidators();
    }

    phoneControl.updateValueAndValidity();
  }

  save() {
    console.log(this.customerForm);
    console.log('Saved: ', JSON.stringify(this.customerForm.value));
  }
}
