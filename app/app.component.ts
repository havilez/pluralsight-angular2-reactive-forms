import { Component } from '@angular/core';

@Component({
    selector: 'my-app',
    template: `
        <div class="container">
          <reactive-signup></reactive-signup>
        </div>
    `
    // template: `
    //     <div class="container">
    //       <my-signup></my-signup>
    //     </div>
    // `
})
export class AppComponent { }
